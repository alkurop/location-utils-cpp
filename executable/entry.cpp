#include <iostream>
#include <locationUtils.hpp>

using namespace std;
using namespace location_utils;

int main(int args, char **argv)
{

        double ne[2];
        ne[0] = 10;
        ne[1] = 20;

        double sw[2];
        sw[0] = 30;
        sw[1] = 40;

        multimap<string, vector<double>> resultMap = convertToBounds(ne, sw);

        typedef multimap<string, vector<double>> MapType;
        typedef multimap<string, vector<double>> PairType;

        double latBounds[resultMap.count(KEY_LAT_BOUNDS)][2];
        double lngBounds[resultMap.count(KEY_LNG_BOUNDS)][2];

        MapType::iterator it;

        pair<MapType::iterator, MapType::iterator> latRange = resultMap.equal_range(KEY_LAT_BOUNDS);
        int latCounter = 0;
        for (it = latRange.first; it != latRange.second; ++it)
        {
                auto value = (*it).second;
                double bound[] = {value[0], value[1]};
                latBounds[latCounter][0] = bound[0];
                latBounds[latCounter][1] = bound[1];
                latCounter++;
        }
        MapType::iterator it1;

        pair<MapType::iterator, MapType::iterator> lngRange = resultMap.equal_range(KEY_LNG_BOUNDS);
        int lngCounter = 0;
        for (it1 = lngRange.first; it1 != lngRange.second; ++it1)
        {
                auto value = (*it).second;
                double bound[] = {value[0], value[1]};
                lngBounds[lngCounter][0] = bound[0];
                lngBounds[lngCounter][1] = bound[1];
                lngCounter++;
        }

         cout << "size " + to_string(sizeof(latBounds[0])/sizeof(latBounds[0][0])) << endl;
         cout << "size " + to_string(sizeof(latBounds[1])/sizeof(latBounds[1][0])) << endl;

        cout << "bound2 " + to_string(latBounds[0][1]) << endl;
        cout << "bound3 " + to_string(lngBounds[0][0]) << endl;
        cout << "bound4 " + to_string(lngBounds[0][1]) << endl;
}