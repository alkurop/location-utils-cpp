#include <iostream>
#include "gtest/gtest.h"
#include <locationUtils.hpp>

using namespace std;
using namespace location_utils;

TEST(LocationUtils_convertToBounds, both_points_above_zero_sw_greater_ne)
{
    double sw[2] = {20, 120};
    double ne[2] = {10, 100};

    auto resultMap =  convertToBounds(ne, sw);
    auto resultVector = convertResultToVectors(resultMap);

    vector<vector<double>> latBounds = resultVector[0];
    vector<vector<double>> lngBounds = resultVector[1];

    ASSERT_EQ(1, latBounds.size());
    ASSERT_EQ(1, lngBounds.size());

    for (auto bound : latBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }

    for (auto bound : lngBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }
}

TEST(LocationUtils_convertToBounds, both_points_above_zero_ne_greater_sw)
{
    double ne[2] = {20, 120};
    double sw[2] = {10, 100};

    auto resultMap = convertToBounds(ne, sw);
    auto resultVector = convertResultToVectors(resultMap);

    vector<vector<double>> latBounds = resultVector[0];
    vector<vector<double>> lngBounds = resultVector[1];

    ASSERT_EQ(1, latBounds.size());
    ASSERT_EQ(1, lngBounds.size());

    for (auto bound : latBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }

    for (auto bound : lngBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }
}

TEST(LocationUtils_convertToBounds, both_points_below_zero_sw_greater_ne)
{
    double ne[2] = {-10, -120};
    double sw[2] = {-2, -100};

    auto resultMap = convertToBounds(ne, sw);
    auto resultVector = convertResultToVectors(resultMap);

    vector<vector<double>> latBounds = resultVector[0];
    vector<vector<double>> lngBounds = resultVector[1];

    ASSERT_EQ(1, latBounds.size());
    ASSERT_EQ(1, lngBounds.size());

    for (auto bound : latBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }

    for (auto bound : lngBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }
}

TEST(LocationUtils_convertToBounds, both_points_below_zero_ne_greater_sw)
{
    double ne[2] = {-10, -100};
    double sw[2] = {-20, -120};

    auto resultMap =  convertToBounds(ne, sw);
    auto resultVector =  convertResultToVectors(resultMap);

    vector<vector<double>> latBounds = resultVector[0];
    vector<vector<double>> lngBounds = resultVector[1];

    ASSERT_EQ(1, latBounds.size());
    ASSERT_EQ(1, lngBounds.size());

    for (auto bound : latBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }

    for (auto bound : lngBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }
}

TEST(LocationUtils_convertToBounds, ne_above_sw_below_zero_in_range)
{
    double ne[2] = {-10, 10};
    double sw[2] = {-20, 20};

    auto resultMap =  convertToBounds(ne, sw);
    auto resultVector =  convertResultToVectors(resultMap);

    vector<vector<double>> latBounds = resultVector[0];
    vector<vector<double>> lngBounds = resultVector[1];

    ASSERT_EQ(1, latBounds.size());
    ASSERT_EQ(1, lngBounds.size());

    for (auto bound : latBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }

    for (auto bound : lngBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
    }
}

TEST(LocationUtils_convertToBounds, ne_above_sw_below_zero_out_of_range) {
    double ne[2] = {-75, -100};
    double sw[2] = {75, 100};

    auto resultMap = convertToBounds(ne, sw);
    auto resultVector = convertResultToVectors(resultMap);

    vector<vector<double>> latBounds = resultVector[0];
    vector<vector<double>> lngBounds = resultVector[1];

    ASSERT_EQ(2, latBounds.size());
    ASSERT_EQ(2, lngBounds.size());

    for (auto bound : latBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
        cout << "lat from " + to_string(bound[0]) + " to " + to_string(bound[1]) << endl;
    }

    for (auto bound : lngBounds)
    {
        ASSERT_EQ(2, bound.size());
        ASSERT_TRUE(bound[0] < bound[1]);
        cout << "lon from " + to_string(bound[0]) + " to " + to_string(bound[1]) << endl;
    }
}
