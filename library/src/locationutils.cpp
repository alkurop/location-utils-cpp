#include "../include/locationUtils.hpp"
#include <math.h>
#include <iostream>

namespace
{
const double LAT_BOUNDS = atan(sinh(M_PI)) * 180 / M_PI;
const double LNG_BOUNDS = 180;

double convert(double arg, const double bounds)
{
    double result;
    if (arg < 0)
    {
        double match = bounds * -1;
        if (arg < match)
        {
            result = arg - match * 2;
        }
        else
        {
            result = arg;
        }
    }
    else if (arg > 0)
    {
        if (arg > bounds)
        {
            result = arg - bounds * 2;
        }
        else
            result = arg;
    }
    else
        result = 0;
    return result;
}

set<vector<double>> calculateBounds(double firstArg, double secondArg, const double bounds)
{
    set<vector<double>> resultSet;
    vector<double> result;
    vector<double> secondResult;

    double first = convert(firstArg, bounds);
    double second = convert(secondArg, bounds);

    if ((first <= 0 && second <= 0) || (first >= 0 && second >= 0))
    {
        double lower = fmin(first, second);
        double higher = fmax(first, second);
        result.push_back(lower);
        result.push_back(higher);
    }
    else if ((first >= 0 && second <= 0) || (first <= 0 && second >= 0))
    {
        double belowZero = fmin(first, second);
        double aboveZero = fmax(first, second);
        double distance = -1 * belowZero + aboveZero;
        if (distance > bounds)
        {
            result.push_back(-1 * bounds);
            result.push_back(belowZero);

            secondResult.push_back(aboveZero);
            secondResult.push_back(bounds);
            resultSet.insert(secondResult);
        }
        else
        {
            result.push_back(belowZero);
            result.push_back(aboveZero);
        }
    }
    resultSet.insert(result);
    return resultSet;
};
}

vector<vector<vector<double>>> location_utils::convertResultToVectors(multimap<string, vector<double>> args)
{
    vector<vector<double>> latBounds;
    vector<vector<double>> lngBounds;
    vector<vector<vector<double>>> resultVector;

    typedef multimap<string, vector<double>> MapType;
    MapType::iterator it;

    auto latRange = args.equal_range(location_utils::KEY_LAT_BOUNDS);
    auto lngRange = args.equal_range(location_utils::KEY_LNG_BOUNDS);
    for (it = latRange.first; it != latRange.second; ++it)
    {
        auto value = (*it).second;
        latBounds.push_back(value);
    }

    for (it = lngRange.first; it != lngRange.second; ++it)
    {
        auto value = (*it).second;
        lngBounds.push_back(value);
    }
    resultVector.push_back(latBounds);
    resultVector.push_back(lngBounds);
    return resultVector;
}


multimap<string, vector<double>> location_utils::convertToBounds(double ne[], double sw[])
{
    double neLat = ne[0];
    double swLat = sw[0];
    double neLon = ne[1];
    double swLon = sw[1];

    auto boundsLat = calculateBounds(neLat, swLat, LAT_BOUNDS);
    auto boundsLng = calculateBounds(neLon, swLon, LNG_BOUNDS);

    multimap<string, vector<double>> resultMap;

    for (vector<double> bound : boundsLat)
    {
        resultMap.emplace(KEY_LAT_BOUNDS, bound);
    }
    for (vector<double> bound : boundsLng)
    {
        resultMap.emplace(KEY_LNG_BOUNDS, bound);
    }
    return resultMap;
}

vector<vector<double>> location_utils::convertProjectionToNeSw(
    double topRight[],
    double topLeft[],
    double bottomRight[],
    double bottomLeft[])
{
    double maxLat = fmax(fmax(topRight[0], topLeft[0]), fmax(bottomRight[0], bottomLeft[0]));
    double minLat = fmin(fmin(topRight[0], topLeft[0]), fmin(bottomRight[0], bottomLeft[0]));

    double maxLng = fmax(fmax(topRight[1], topLeft[1]), fmax(bottomRight[1], bottomLeft[1]));
    double minLng = fmin(fmin(topRight[1], topLeft[1]), fmin(bottomRight[1], bottomLeft[1]));

    vector<double> se;
    se.push_back(maxLat);
    se.push_back(maxLng);

    vector<double> nw;
    nw.push_back(minLat);
    nw.push_back(minLng);

    vector<vector<double>> resultVector;
    resultVector.push_back(se);
    resultVector.push_back(nw);

    return resultVector;
}
