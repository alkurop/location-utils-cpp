#ifndef __LOCATION_UTILS__
#define __LOCATION_UTILS__

#include <map>
#include <set>
#include <vector>
#include <string>

using namespace std;
namespace location_utils{

  static const string KEY_LAT_BOUNDS = "lat";
  static const string KEY_LNG_BOUNDS = "lng";

  vector<vector<double>> convertProjectionToNeSw(
      double topRight[],
      double topLeft[],
      double bottomRight[],
      double bottomLeft[]
      );

  multimap<string, vector<double>> convertToBounds(double ne[], double sw[]);
  vector<vector<vector<double>>> convertResultToVectors(multimap<string, vector<double>> args);
};

#endif